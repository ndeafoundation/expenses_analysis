/**
accounts - list of account ids, comma separated
aggregator_type - one of: day, month, week
since - date in SO 8601 UTC date time format
until - date in SO 8601 UTC date time format
**/
function getData(nordeaAccessToken , accounts, aggregator_type, since, until, callback){
	var result = [];
	var expensesLoaded = false;
	var incomesLoaded = false;
	getExpenses(nordeaAccessToken, accounts, aggregator_type, since, until, function(expensesData){
		for(var expenseElem in expensesData){
			amount = expensesData[expenseElem].amount;
			date = expensesData[expenseElem].period;
			if(result[date]){
				result[date]["out"] = amount;
			}
			else{
				result[date] = {"out":amount};
			}
		}
		expensesLoaded= true;
		if(expensesLoaded&&incomesLoaded){
			callback(result);
		}
	});
			
	getIncomes(nordeaAccessToken, accounts, aggregator_type, since, until, function(incomesData){

		for(var incomeElem in incomesData){
			amount = incomesData[incomeElem].amount;
			date = incomesData[incomeElem].period;
			
			if(result[date]){
				result[date]["in"] = amount;
			}
			else{
				result[date] = {"in":amount};
			}
		}
		incomesLoaded= true;
		if(expensesLoaded&&incomesLoaded){
			callback(result);
		}
	});
}


/**
accounts - list of account ids, comma separated
aggregator_type - one of: day, month, week
since - date in SO 8601 UTC date time format
until - date in SO 8601 UTC date time format
**/
function getExpenses(accessToken, accounts, aggregator_type, since, until, callback){
	var uri= 'https://api.test.nordea.ninja/v0.4/banking/expenses_by_date';
	if(since){
		uri = addParamToUrl(uri,'X-Since',since);
	}
	if(until){
		uri = addParamToUrl(uri,'X-Until',until);
	}
	uri = addParamToUrl(uri,'aggregator',aggregator_type);
	
	$.ajax({
    url: uri,
    type: 'GET',
    headers: { Authentication: accessToken, accountids: accounts },
    success: function(response) { callback(response); }
});
}

/**
accounts - list of account ids, comma separated
aggregator_type - one of: day, month, week
since - date in SO 8601 UTC date time format
until - date in SO 8601 UTC date time format
**/
function getIncomes(accessToken, accounts, aggregator_type, since, until, callback){
	var uri= 'https://api.test.nordea.ninja/v0.4/banking/incomes_by_date';
	if(since){
		uri = addParamToUrl(uri,'X-Since',since);
	}
	if(until){
		uri = addParamToUrl(uri,'X-Until',until);
	}
	uri = addParamToUrl(uri,'aggregator',aggregator_type);
	
	$.ajax({
    url: uri,
    type: 'GET',
    headers: { Authentication: accessToken, accountids: accounts },
    success: function(response) { callback(response); }
});
}

function addParamToUrl(base, key, value) {
    var sep = (base.indexOf('?') > -1) ? '&' : '?';
    return base + sep + key + '=' + value;
}

function sortData(elems){
	result = [];
	keys = [];
  
	for (k in elems) {
		keys.push(k);
	}

	keys.sort(function(a,b){
		return getDateForSort(a)-getDateForSort(b);
	});
	
len = keys.length;

	for (i = 0; i < len; i++) {
		k = keys[i];
		newObj = { date: k, in : elems[k].in, out: elems[k].out};
		result.push(newObj);
	}
return result;
}

function getDateForSort(date){
	var splitted = date.split(".");
	if(splitted.length == 3){
		return addZero(splitted[2])+addZero(splitted[1])+addZero(splitted[0]);
	}
	else{
		return addZero(splitted[1])+addZero(splitted[0]);
	}
}

function addZero(number){
	if (parseInt(number)<10){
		return "0"+parseInt(number);
	}
	else{
		return number;
	}
}

/** usage example

getData('Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkYmYiLCJhdWQiOlsib3NsIl0sImp0aSI6IlRFU1QtMTQ0NTUwNDYxMzk3MCIsImNsaWVudF9pZCI6IjExMTExMDA1IiwiY291bnRyeSI6IlNFIiwiYW0iOiJCQU5LSUQiLCJhbCI6IkhJR0giLCJpYXQiOjE0NDU1MDQ2MTcsImV4cCI6MTQ0NTUwNDkxNywiY2giOiJSQk8iLCJncmFudHMiOnsiYWdyZWVtZW50IjoyMTcyMzMyfX0.kb-KBAEdszImaqg-nTQqwhMy8igN-krLD06q5vqmlFcTGIWG22HuG2no851ZiIaODUDZSNPMp1h9Qyw3yYh6GVmL3KU-1CpIAK14tsBSfdUEupTgBzWfr7SmqXjUPGmjiU2-CzedlphGRcMLQQJNQZy-X61R3Ed7aObHIXbh2U_IoD2_-pGShbv25aAGQojG5O3NhbRk4AnWvT1qFQz8bg5GMR9aLC2cYXjInOVy_vvh-mIHps2nsOJRcEgzZgDy1E42vc1Rr8wrzpDT9z_nB1bCn0jjGsITtGvi2RbGrF6DaWYPe6fB0_Q170A2ayozJ0UDwxFeeZDMA3tuAD2ezO_APMMvliUeu75xfjp-33OoH3SG0NmHOo0JzZNM2gG2of-dPt-pLQK44gKZJoc5SZ9jmgxrqSEUG06R8Hp3dbo_PBC3NE-2wtS724OPzeFe-_Lc-brTS8wUvKhqVbsvdmiL2SOcSZ2Z-VOfWDdbGxsoKQDu5XV6nP1POauRYJcq-gg2Ywdjj1BG4p1XdIA6e3OJzJc6pbfYvZrlImjeQS3b2BE8quldwWwaKnFg9Xase7gVabTFcxdXoqSnGSJa1C_f-xjSci48NEUKJcUXDqDbJ5kQZzenZn7JY79O9rC6hx8-8bPhRgGe-t2ASZ4pHQAWVRwjC0kPnoJlTI6MLmU' , 'NAID-SE-EUR-40644040538', 
				'day', 
				'2014-03-01T13:00:00Z', 
				'2015-06-01T13:00:00Z', 
				function(data){result=sortData(data);
											 for (i = 0; i < result.length; i++) {
											 	console.log(result[i].date);
											 }
											});

**/