angular.module('myApp', ['chartjs-directive'])
  .controller('myController', function($scope, $rootScope){


chartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
        {
						first: true,
						type:"bar",
            label: "My First dataset",
            fillColor: "rgba(98,187,70,0.5)",
            strokeColor: "rgba(98,187,70,0.8)",
            highlightFill: "rgba(98,187,70,0.75)",
            highlightStroke: "rgba(98,187,70,1)",
            data: [65, 59, 80, 81, 56, 50, 40]
        },
        {
						type:"bar",
            label: "My Second dataset",
            fillColor: "rgba(148,148,148,0.5)",
            strokeColor: "rgba(148,148,148,0.8)",
            highlightFill: "rgba(148,148,148,0.75)",
            highlightStroke: "rgba(148,148,148,1)",
            data: [-28, -48, -40, -19, -86, -27, -90]
        },
			        {
						type:"line",
            label: "My Second dataset",
            fillColor: "rgba(98,187,70,0.0)",
            strokeColor: "rgba(98,187,70,0.8)",
            highlightFill: "rrgba(98,187,70,0.75)",
            highlightStroke: "rgba(98,187,70,1)",
            data: [30, -20, 35, -28, 13, 22, -60]
        }
    ]
};

   $scope.myChart = {"data": chartData, "options": { "scaleBeginAtZero" : false} };
	
	
	
}
						 
						 );

