angular.module('myApp', [])
  .controller('MyController', function($scope, $rootScope){


chartData = {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [

    ]
};

    $scope.getAccounts = function() {
    $.ajax({
        url: "https://api.nordea.ninja/v0.4/banking/accounts",
        type: "GET",
        headers: {"Authentication": $scope.textModel},
        success: function(results){
             var accountsList = [];
             var accountsToggleList = [];
             $.each(results, function(index,item) {    
                accountsList.push({"id": item.id, "balance": item.balance});
                accountsToggleList.push({"id": item.id, "toggle":true});
                });
                    $scope.accounts = accountsList;
                    $scope.accountsToggle = accountsToggleList;
                    $scope.$apply();
       
        }
       
    });
  };

  $scope.generateChart = function(aggregator_type) {
    aggregatorType = aggregator_type;
    // console.log(aggregator_type);
    // console.log($scope.accountsToggle);
    // console.log($scope.accounts);

    var accountsString = "";
    for (x in $scope.accountsToggle){
        if ($scope.accountsToggle[x].toggle == true) {
            accountsString += $scope.accountsToggle[x].id;
            accountsString += ",";
        }
    }
    if (!accountsString=="") {
        accountsString = accountsString.substr(0, accountsString.length-1);

        //get data and regenerate chart
        var endDate = new Date().toISOString();
        var date = new Date();
        var startDate = '';
        switch (aggregator_type){
            case "month" : 
                date.setMonth(date.getMonth() - 5);
                startDate = date; 
                $scope.currentView = "month";
              //  $scope.$apply();
                break;
            case "week" :
                date.setMonth(date.getMonth() - 2); 
                startDate = date; 
                $scope.currentView = "week";
              //  $scope.$apply();
                break;
            case "day" : 
                date.setDate(date.getDate() - 14); 
                startDate = date;
                $scope.currentView = "day";
               // $scope.$apply();
                break;
        }

        console.log($scope.accessToken, 
                    $scope.client,
                    //$scope.textModel,//'Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkYmYiLCJhdWQiOlsib3NsIl0sImp0aSI6IlRFU1QtMTQ0ODQzNzkyOTMxNiIsImNsaWVudF9pZCI6Ijk1IiwiY291bnRyeSI6IlNFIiwiYW0iOiJCQU5LSUQiLCJhbCI6IkhJR0giLCJpYXQiOjE0NDg0NTg4NjksImV4cCI6MTQ0ODQ1OTE2OSwiY2giOiJSQk8iLCJncmFudHMiOnsiYWdyZWVtZW50IjoyMTcyMzMyfX0.Mm9QacM0uUXRbLqepPpZH4sVEmdLYrgL286pogSuljfegzWeiXhqhCL0PWFbXv50Tih0Fqbyj0TjWdWilUCEcMJGeJSTa9swVvOW2Kxh8jz1Ho5PVR8E6ElKF8RA5CixF0-XyEOGZoRSGIsq9kDO4daDDNw8esMJC41sVx_xkpXDvJNIfC5Xc4TBtPWcsCKphcuXyLeByxN27Sv-BHqYjkb_9k64hEQEhYm4_sD-pwiK_qu544tnEP0ceJyvUbBbnv7BQ-H4zXLyh59OLd9CzhxH79It08mN2x9mpNgVhMdpRK2kJomxwdeduzG1di6zsuIbLQEGNu9Z5U0sEQS-AmTW3eM91Ks56nECK9jvKeJbWUOdshAkBqHPuRGcHsuKQcX_vsz2aEunF3060OgdvFDw27IHMHYIORXHot6X82kRxkFjWYHegipVhaE5f5Wtm6ju1lbZ72occaOsRcDRNmnvajUhYVsndoZ4TFZsMZjaoyJBi3aj-5dMlUGPmGy-TekDj5CHpvyeZs1QI6RzvD3lSLjxeWQVpesU2S1Ngj3kcM-arsgjcBFkbGSG4kVkKEvBnHYRx6ltqgu5xdjnNQvpYBzl4wAMNJuFhMbyqQzBkdMzqKpqk6dklcigGGGBfORJ_topgNQpATjygOQBQ95nPUE8WQdEDPvrLzEuvfA',
                    accountsString, 
                    aggregator_type, 
                    startDate.toISOString(), 
                    endDate);
        getData(
                    $scope.textModel,//'Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkYmYiLCJhdWQiOlsib3NsIl0sImp0aSI6IlRFU1QtMTQ0ODQzNzkyOTMxNiIsImNsaWVudF9pZCI6Ijk1IiwiY291bnRyeSI6IlNFIiwiYW0iOiJCQU5LSUQiLCJhbCI6IkhJR0giLCJpYXQiOjE0NDg0NTg4NjksImV4cCI6MTQ0ODQ1OTE2OSwiY2giOiJSQk8iLCJncmFudHMiOnsiYWdyZWVtZW50IjoyMTcyMzMyfX0.Mm9QacM0uUXRbLqepPpZH4sVEmdLYrgL286pogSuljfegzWeiXhqhCL0PWFbXv50Tih0Fqbyj0TjWdWilUCEcMJGeJSTa9swVvOW2Kxh8jz1Ho5PVR8E6ElKF8RA5CixF0-XyEOGZoRSGIsq9kDO4daDDNw8esMJC41sVx_xkpXDvJNIfC5Xc4TBtPWcsCKphcuXyLeByxN27Sv-BHqYjkb_9k64hEQEhYm4_sD-pwiK_qu544tnEP0ceJyvUbBbnv7BQ-H4zXLyh59OLd9CzhxH79It08mN2x9mpNgVhMdpRK2kJomxwdeduzG1di6zsuIbLQEGNu9Z5U0sEQS-AmTW3eM91Ks56nECK9jvKeJbWUOdshAkBqHPuRGcHsuKQcX_vsz2aEunF3060OgdvFDw27IHMHYIORXHot6X82kRxkFjWYHegipVhaE5f5Wtm6ju1lbZ72occaOsRcDRNmnvajUhYVsndoZ4TFZsMZjaoyJBi3aj-5dMlUGPmGy-TekDj5CHpvyeZs1QI6RzvD3lSLjxeWQVpesU2S1Ngj3kcM-arsgjcBFkbGSG4kVkKEvBnHYRx6ltqgu5xdjnNQvpYBzl4wAMNJuFhMbyqQzBkdMzqKpqk6dklcigGGGBfORJ_topgNQpATjygOQBQ95nPUE8WQdEDPvrLzEuvfA',
                    accountsString, 
                    aggregator_type, 
                    startDate.toISOString(), 
                    endDate, 
                    redraw);

        // narrow results by
        console.log(toggleLiquidity);
        console.log(toggleLiquidityAlert);
        console.log(toggleIncome);
        console.log(toggleExpenses);

    } else {
        console.log("dupa");
    }
  };

  $scope.toggleLiquidity = function () {
    toggleLiquidity = !toggleLiquidity;
    console.log("toggleLiquidity "+toggleLiquidity);
  };

  $scope.toggleLiquidityAlert = function () {
    toggleLiquidityAlert = !toggleLiquidityAlert;
    console.log("toggleLiquidityAlert "+toggleLiquidityAlert);
  };

  $scope.toggleIncome = function () {
    toggleIncome = !toggleIncome;
	toggleDataset('income', toggleIncome);
  };

  $scope.toggleExpenses = function () {
    toggleExpenses = !toggleExpenses;
	toggleDataset('expenses', toggleExpenses);

  };

   $scope.toggleAccount = function (accountName) {
    for (var x in $scope.accountsToggle){
        if ($scope.accountsToggle[x].id == accountName)
        {
            $scope.accountsToggle[x].toggle = !$scope.accountsToggle[x].toggle;
            console.log("toggleAccount "+$scope.accountsToggle[x].id + " " +$scope.accountsToggle[x].toggle);
        }
    }
    $scope.generateChart($scope.currentView);
    // toggleDataset('expenses', toggleExpenses);
    // toggleDataset('income', toggleIncome);
  };
    

var toggleLiquidity = true;
var toggleLiquidityAlert = false;
var toggleIncome = true;
var toggleExpenses = true;
var aggregatorType = "month";

$scope.accountsToggle = [];
$scope.aggregator_type = { month:"month", week:"week", day:"day"};
$scope.accounts = [];
$scope.textModel = "Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkYmYiLCJhdWQiOlsib3NsIl0sImp0aSI6IlRFU1QtMTQ0NTUwNDYxMzk3MCIsImNsaWVudF9pZCI6IjExMTExMDA1IiwiY291bnRyeSI6IlNFIiwiYW0iOiJCQU5LSUQiLCJhbCI6IkhJR0giLCJpYXQiOjE0NDU1MDQ2MTcsImV4cCI6MTQ0NTUwNDkxNywiY2giOiJSQk8iLCJncmFudHMiOnsiYWdyZWVtZW50IjoyMTcyMzMyfX0.kb-KBAEdszImaqg-nTQqwhMy8igN-krLD06q5vqmlFcTGIWG22HuG2no851ZiIaODUDZSNPMp1h9Qyw3yYh6GVmL3KU-1CpIAK14tsBSfdUEupTgBzWfr7SmqXjUPGmjiU2-CzedlphGRcMLQQJNQZy-X61R3Ed7aObHIXbh2U_IoD2_-pGShbv25aAGQojG5O3NhbRk4AnWvT1qFQz8bg5GMR9aLC2cYXjInOVy_vvh-mIHps2nsOJRcEgzZgDy1E42vc1Rr8wrzpDT9z_nB1bCn0jjGsITtGvi2RbGrF6DaWYPe6fB0_Q170A2ayozJ0UDwxFeeZDMA3tuAD2ezO_APMMvliUeu75xfjp-33OoH3SG0NmHOo0JzZNM2gG2of-dPt-pLQK44gKZJoc5SZ9jmgxrqSEUG06R8Hp3dbo_PBC3NE-2wtS724OPzeFe-_Lc-brTS8wUvKhqVbsvdmiL2SOcSZ2Z-VOfWDdbGxsoKQDu5XV6nP1POauRYJcq-gg2Ywdjj1BG4p1XdIA6e3OJzJc6pbfYvZrlImjeQS3b2BE8quldwWwaKnFg9Xase7gVabTFcxdXoqSnGSJa1C_f-xjSci48NEUKJcUXDqDbJ5kQZzenZn7JY79O9rC6hx8-8bPhRgGe-t2ASZ4pHQAWVRwjC0kPnoJlTI6MLmU";
//$scope.textModel = "Bearer eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJkYmYiLCJhdWQiOlsib3NsIl0sImp0aSI6IlRFU1QtMTQ0ODYwOTIzMjUyMiIsImNsaWVudF9pZCI6IjQiLCJjb3VudHJ5IjoiU0UiLCJhbSI6IkJBTktJRCIsImFsIjoiSElHSCIsImlhdCI6MTQ0ODYwOTY2OSwiZXhwIjoxNDQ4NjA5OTY5LCJjaCI6IlJCTyIsImdyYW50cyI6eyJhZ3JlZW1lbnQiOjIxNzIzMzJ9fQ.Zf2TTOG5LaNRlenNbaq4WVlYkAXaBuY7G_5Qc4hNk4epiZRTy1bp6o666eyLS7w4DRP5B4mITJ747Qwpun5wZx594XmxBUG67BrIzRniE6gyJg0K5iw-rYoWvQS1u-a2nLdS-ozjKmC5_Lhvgn42OOhOxg0rH72xrDzWtyk1OSuSvRhzz0ra9CdMCjPMnaJI1bq2f9fSpUILuK3Pmlnmm5L703azVXg8JvqtFMp6MwoVTNGbozulG3sigW7fhosN8FtlechVH3nCpMl75ax1ZL9oODZju1rZpQ487tVpLVSKt19utb4lLmn4wmPGzDdX2zWFr7tcLAq8MMdXr0XMZsyDqGBo8b1j9ZFzaIJYm4FfgYQx_ITYopVeAr5lBO5F_zAzPHOK7ajGpSFoIZyFVWb1pWiJbcFVNE5nGTXhKy3vcS2TYjRWUhBaFBbSeLXouN8Him-JvHZxXi6qhCBRkHwI1-rr0qsbanGxiTsmOOQwzvPWulKRgiaQubGFJo4-nA0wOvGTOrOxwMhcFwyyNVmUPORi0kg_vY9np1Yg5uvkBzBRm-t5_6vGFvoC4MmqH9gLspYBxOOtPglA-6FVlCW6rxAcFPvg3DEuQ-rsGHLco3Kn29lzZN98iGt3HPsK4dl-bPRKoqop7RLRy6K5ieoX4hs5cUIDW-fq4YYO7DM";
$scope.accessToken = "8e4a77f71e93c85f2a9e630854223635";
$scope.client = "95";
$scope.currentView = "month";

$scope.myChart = {"data": chartData, "options": { "scaleBeginAtZero" : false} };
	
	var chart;
	var innerChart;
	var datasets;
	
	function initChart() {
		if (typeof chart === 'undefined') {
			var canvas = document.getElementById('myChart');
			console.log(canvas);
			var context = canvas.getContext('2d');
			canvas.width = 600;
			canvas.height = 400;

			var options = {
				type:   "LineBar",
				width:  600,
				height: 400,
			};
			chart = new Chart(context, options);	
		}
	}
	
redraw = function(data) {

	initChart();
	
	var inValues = [], outValues = [];
	
    var keys = [];

    data=sortData(data);
    for (i = 0; i < result.length; i++) {
        key = data[i].date;
        keys.push(key);
        console.log(data[i].date);
        inValues.push(typeof data[i]['in'] === 'undefined' ? 0 : data[i]['in']);
        outValues.push(typeof data[i]['out'] === 'undefined' ? 0 : data[i]['out']);
    }

    var chartData = {
    labels: keys,
    datasets: [
        {
            first: true,
            type:"bar",
            label: "income",
            fillColor: "rgba(98,187,70,0.5)",
            strokeColor: "rgba(98,187,70,0.8)",
            highlightFill: "rgba(98,187,70,0.75)",
            highlightStroke: "rgba(98,187,70,1)",
            data: inValues
                        
        },
        {
            type:"bar",
            label: "expenses",
            fillColor: "rgba(148,148,148,0.5)",
            strokeColor: "rgba(148,148,148,0.8)",
            highlightFill: "rgba(148,148,148,0.75)",
            highlightStroke: "rgba(148,148,148,1)",
            data: outValues
                        
        }/*,
        {
            type:"line",
            label: "My Second dataset",
            fillColor: "rgba(98,187,70,0.0)",
            strokeColor: "rgba(98,187,70,0.8)",
            highlightFill: "rrgba(98,187,70,0.75)",
            highlightStroke: "rgba(98,187,70,1)",
            data: [30, -20, 35, -28, 13, 22, -60]
        }*/
    ]
};
    
	if (typeof innerChart !== 'undefined') {
		innerChart.destroy();
	}
	innerChart = chart.LineBar(chartData,{'scaleBeginAtZero' : false});
	
	datasets = [];
	
	for (var i in innerChart.datasets) {
		
		datasets[innerChart.datasets[i].label] = {'bars' : []};
		for (var j in innerChart.datasets[i].bars) {
			datasets[innerChart.datasets[i].label].bars[j] = innerChart.datasets[i].bars[j].value;	
		}
		
	}
	
	toggleDataset('income', toggleIncome);
	toggleDataset('expenses', toggleExpenses);
}

function findDataset(dataset, label) {
	var found = null;
	dataset.forEach(function(element, index, array) {
	
		if (element.label === label) {
			found = index;
		}
	});
	
	return found !== null ? found : -1;
}

function toggleDataset(dataset, toggle) {
	
	var found = findDataset(innerChart.datasets, dataset);
	if (!toggle) {
			for (var i in innerChart.datasets[found].bars) {
				innerChart.datasets[found].bars[i].value = 0;
			}
	} else {	
			for (var i in innerChart.datasets[found].bars) {
				console.log(datasets[dataset]);
				innerChart.datasets[found].bars[i].value = datasets[dataset].bars[i];
			}
	}
	innerChart.update();
}



});

